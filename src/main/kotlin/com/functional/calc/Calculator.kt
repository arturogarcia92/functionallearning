package com.functional.calc

class Calculator(
    private val calculation: (Operand, Operand) -> Operand,
    private val roundMode: (Operand) -> Operand
) {

    fun doCalculation(firstValue: Operand, secondValue: Operand): Double =
        calculation(firstValue, secondValue)
            .round(roundMode)
            .value

}

data class Operand(val value: Double) {
    fun round(roundMode: (Operand) -> Operand) =
        roundMode(this)
}


object Sum {
    fun doSum(firstOperand: Operand, secondOperand: Operand): Operand =
        Operand(firstOperand.value + secondOperand.value)
}

val sum: (Operand, Operand) -> (Operand) = { x, y -> Operand(x.value + y.value) }
val normalRound: (Operand) -> Operand = { x -> x }
val ceilingRound: (Operand) -> Operand = { x -> Operand(Math.ceil(x.value)) }
