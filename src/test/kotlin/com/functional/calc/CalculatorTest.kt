package com.functional.calc

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class CalculatorTest {

    @Test
    fun `when Sum decimals number with normalRound the result must be a normal sum using val`() {

        val expected = Calculator(sum, normalRound).doCalculation(Operand(3.5), Operand(3.6))

        assertEquals(7.1, expected)
    }

    @Test
    fun `when Sum decimals number with normalRound the result must be a normal sum using class`() {

        val expected = Calculator(Sum::doSum, normalRound).doCalculation(Operand(3.5), Operand(3.6))

        assertEquals(7.1, expected)
    }

    @Test
    fun `when Sum decimals number with ceilingRound the result must be a ceil result`() {

        val expected = Calculator(Sum::doSum, ceilingRound).doCalculation(Operand(3.5), Operand(3.6))

        assertEquals(8.0, expected)
    }


}
